package com.adf.tugasakhir.dataclass;

import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Setter @Getter
@Table(name = "papers")
public class Paper {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;
    private String abstrak;
    private String url;

    public Paper(String title, String url, String abstrak) {
        this.title = title;
        this.abstrak = abstrak;
        this.url = url;
    }

    @Override
    public String toString() {
        return "Paper{" + "title='" + title + '\'' + ", abstrak='" + abstrak + '\'' + ", url='" + url + '\'' + '}';
    }
}
